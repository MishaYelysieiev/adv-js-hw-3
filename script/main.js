let table = document.createElement('table');
table.classList.add('table');
document.body.appendChild(table);

for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr');
    table.appendChild(tr);
    for (let j = 0; j < 30; j++) {
        let td = document.createElement('td');
        td.classList.add('area');
        tr.appendChild(td);
    }
}

document.addEventListener('click', (e) => {
    if (e.target.tagName === 'TD') {
        e.target.classList.toggle('area_colored');
    } else {
        let colored = document.querySelectorAll('.area');
        colored.forEach((item) => {
            item.classList.toggle('area_colored');
            item.classList.toggle('area_dark-theme');
        });
    }
});